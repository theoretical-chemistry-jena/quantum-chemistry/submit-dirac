#! /usr/bin/env nix-shell
#! nix-shell -i python3 -p "python3.withPackages( ps: with ps; [numpy] )" qchem.dirac

""" S.D.G. """

import logging
import argparse
import multiprocessing
import subprocess
from pathlib import Path


logging.basicConfig(level=logging.INFO)
logger = logging.getLogger(__name__)

def parse_args():

    parser = argparse.ArgumentParser("submit_dirac.py")
    parser.add_argument(
        "--mol",
        "-m",
        dest="molfile",
        required=True,
        type=str,
        help="The molecule file to be read by DIRAC."
    )
    parser.add_argument(
        "--inp",
        "-i",
        dest="inpfile",
        required=True,
        type=str,
        help="The Abacus input file to be read by DIRAC."
    )
    parser.add_argument(
        "--scratchdir",
        type=str,
        default="/scratch",
        dest="scratch",
        help="The scratch directory to be used."
    )
    parser.add_argument(
        "--mpicores",
        type=int,
        default=multiprocessing.cpu_count(),
        dest="mpi",
        help="The scratch directory to be used."
    )
    parser.add_argument(
        "--extra-arg",
        dest="extra_args",
        action="extend",
        nargs="*",
        help="Extra arguments parsed to DIRAC."
    )

    parser.add_argument(
        "--dry-run",
        action="store_true",
        dest="dry_run",
        help="If specified, do not actually run the calculation."
    )

    return parser.parse_args()


def main():
    """
    Main DIRAC spawner
    """

    args = parse_args()

    inpfile = Path(args.inpfile).expanduser().resolve()
    molfile = Path(args.molfile).expanduser().resolve()
    scratch = Path(args.scratch).expanduser().resolve()
    n_cores = args.mpi

    logfile = inpfile.parent / "dirac_log.out"

    cmd = [
        "pam",
        "--inp={}".format(inpfile),
        "--mol={}".format(molfile),
        "--scratch={}".format(scratch),
        "--mpi={}".format(n_cores)
    ]

    cmd.extend(args.extra_args)

    logger.info("The command: " + " ".join(cmd))
    logger.info("Logging DIRAC output to file: {}".format(logfile))

    if not args.dry_run:
        proc = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.STDOUT )

        with open(logfile, "wb") as f:
            f.write(proc.stdout)


if __name__ == "__main__":
    main()