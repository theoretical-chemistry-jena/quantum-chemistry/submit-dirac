# Quantum chemistry with Nix

Within the university network, add the following channel to your local [nix](https://nixos.org) installation:

```bash
nix-channel --add https://troja.ipc3.uni-jena.de/channel/custom/QChem/nixpkgs-unstable_openblas_noavx/qchem.channel qchem
```

You may need to update your `NIX_PATH` respectively to find the user-defined channel:

```bash
echo 'export NIX_PATH=nixpkgs=$HOME/.nix-defexpr/channels/qchem:$HOME/.nix-defexpr/channels:/nix/var/nix/profiles/per-user/root/channels' >> $HOME/.bashrc
```

Then you can work with the respective packages like that:

```bash
nix-shell -p qchem.dirac
```

To start a DIRAC calculation using the script, simply run:

```bash
./submit_dirac.py -i hf.inp -m mol.xyz
```

The help for the script can be found with the following command:

```bash
./submit_dirac.py --help
```
